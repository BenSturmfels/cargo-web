import os

from fabric.api import env, local
from fabric.contrib.project import rsync_project

from vps_deploy.django_fabric import transfer_files_git, update_nginx

env.hosts = ['paris.sturm.com.au']
env.project_dir = '/home/web/cargo'
env.site_name = 'cargo'
env.nginx_conf = 'deploy/nginx.conf'

def deploy():
    """Install or deploy updates this website."""

    transfer_files_git()
    local('make publish')
    rsync_project(
        local_dir='output/', remote_dir=os.path.join(env.project_dir, 'htdocs'))
    update_nginx()
